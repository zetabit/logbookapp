#import "SentryPlugin.h"
#import <Cordova/CDVPlugin.h>
#import <Sentry/Sentry.h>

@implementation SentryPlugin

- (void)pluginInitialize
{
  NSString *dsn = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"SENTRY_DSN"];
  if (dsn == nil) {
    NSLog(@"Please set SENTRY_DSN");
    return;
  }

  NSError *error = nil;
  SentryClient *client = [[SentryClient alloc] initWithDsn:dsn didFailWithError:&error];
  SentryClient.sharedClient = client;
  [SentryClient.sharedClient startCrashHandlerWithError:&error];
  if (error != nil) {
    NSLog(@"%@", error);
  }
}

@end
