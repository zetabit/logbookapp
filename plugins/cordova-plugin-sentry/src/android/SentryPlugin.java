package org.apache.cordova.plugin;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;

import io.sentry.Sentry;
import io.sentry.android.AndroidSentryClientFactory;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;

public class SentryPlugin extends CordovaPlugin {

  @Override
  public void initialize(CordovaInterface cordova, CordovaWebView webView) {
    super.initialize(cordova, webView);

    Activity activity = cordova.getActivity();
    Context ctx = activity.getApplicationContext();

    ApplicationInfo appInfo = null;
    try {
      appInfo = activity.getPackageManager().getApplicationInfo(activity.getPackageName(), PackageManager.GET_META_DATA);
    } catch (NameNotFoundException e) {}

    String sentryDsn = appInfo.metaData.getString("io.sentry:sentry-android.DSN");
    Sentry.init(sentryDsn, new AndroidSentryClientFactory(ctx));
  }

}
