# cordova-plugin-sentry

Cordova plugin for [Sentry](https://sentry.io/).

## Installation

```
cordova plugin add cordova-plugin-sentry --variable SENTRY_DSN="https://<key>@sentry.io/<project>"
```

For iOS you need to [upload debug symbols](https://docs.sentry.io/clients/cocoa/dsym). 

## Supported Platforms

- Android
- iOS
