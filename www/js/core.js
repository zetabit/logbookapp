const TYPE_COMPLECTION = 2, TYPE_SIMILAR = 0;
const CHECKUP_REVIEW = 0, CHECKUP_RECEIPT = 1, CHECKUP_TRANSMIT = 2;
const RECEIPT_NONE = 0, RECEIPT_SERVICE = 1, RECEIPT_DISMISSAL = 2;
const PART_STATUS_ALL_OK = 0, PART_STATUS_COMMENT = 1;

/**
 * Represents state of the app
 * @returns {TState}
 */
var TState = function () {
    //that fiels will be loaded from storage
    this.token = "";
    this.history = [];
    this.uploadQueue = [];
    this.lastImage = {};
    this.data = {};
    this.carPictures = [];
    
    //load some params
    this.load();
    
    //other params may be that, overriding
    this.page = "";
    this.possiblePages = this.initPossiblePages();
    var lastItem = this.lastHistoryItem();
    
    if(lastItem !== null && typeof lastItem !== 'undefined' && typeof lastItem.stateData !== 'undefined') {
        this.data = lastItem.stateData;
    }
    
    if(this.token.length) {
        $.ajaxPrefilter(function( options ) {
            if ( !options.beforeSend) {
                options.beforeSend = function (xhr) { 
                    xhr.setRequestHeader('Api-Token', core.state.token);
                    xhr.setRequestHeader('App-Version', core.version);
                };
            }
        });
    }
};

/**
 * Get serialized obj for storage saving.
 * @param {type} obj , by default, obj = this
 * @returns {String}
 */
TState.prototype.getForStorage = function(obj) {
    // Note: cache should not be re-used by repeated calls to JSON.stringify.
    var cache = [];
    var obj = obj || this;
    var json = JSON.stringify(obj, function (key, value) {
        if (typeof value === 'object' && value !== null) {
            if (cache.indexOf(value) !== -1) {
                // Circular reference found, discard key
                return;
            }
            // Store value in our collection
            cache.push(value);
        }
        return value;
    });
    cache = null; // Enable garbage collection
    return json;
};

/**
 * Possible pages with states
 * @returns {object}
 */
TState.prototype.initPossiblePages = function(){
    return {
        login : {
            page    : 'login',       //system name
            path    : '/index.html', //path name for location in browser
            loaded  : 'loginPageLoaded' //function in core after loaded page
        },
        user_profile : {
            page : 'user_profile',
            path : '/pages/user/profile.html',
            loaded : 'userProfilePageLoaded'
        },
        user : {
            page : 'user',
            path : '/user.html',
            loaded : 'userPageLoaded'
        },
        cars : {
            page : 'cars',
            path : '/cars.html',
            loaded : 'carsPageLoaded'
        },
        cars_add : {
            page : 'cars_add',
            path : '/pages/car/add.html',
            loaded : 'carsAddPageLoaded'
        },
        car_show : {
            page : 'car_show',
            path : '/car.html',
            loaded : 'carPageLoaded'
        },
        car_history : {
            page : 'car_history',
            path : '/pages/car/history.html',
            loaded : 'carHistoryPageLoaded'
        },
        car_inspection : {
            page : 'car_inspection',
            path : '/pages/car/inspection.html',
            loaded : 'carInspectionPageLoaded'
        },
        car_inspection_photo : {
            page : 'car_inspection_photo',
            path : '/pages/car/inspection-photo.html',
            loaded: 'carInspectionPhotoPageLoaded'
        },
        car_inspection_camera : {
            page : 'car_inspection_camera',
            path : '/pages/car/inspection-camera.html',
            loaded: 'carInspectionCameraPageLoaded'
        },
        car_inspection_camera_done : {
            page : 'car_inspection_camera_done',
            path : '/pages/car/inspection-camera-done.html',
            loaded: 'carInspectionCameraDonePageLoaded'
        },
        car_inspection_photo_again : {
            page : 'car_inspection_photo_again',
            path : '/pages/car/inspection-photo-again.html',
            loaded: 'carInspectionPhotoAgainPageLoaded'
        },
        car_inspection_complection : {
            page : 'car_inspection_complection',
            path : '/pages/car/inspection-complection.html',
            loaded: 'carInspectionComplectionPageLoaded'
        },
        car_inspection_camera_total : {
            page : 'car_inspection_camera_total',
            path : '/pages/car/inspection-camera-total.html',
            loaded: 'carInspectionCameraTotalPageLoaded'
        },
        car_inspection_camera_total_done : {
            page : 'car_inspection_camera_total_done',
            path : '/pages/car/inspection-camera-total-done.html',
            loaded: 'carInspectionCameraTotalDonePageLoaded'
        },
        car_inspection_camera_order_outfit : {
            page : 'car_inspection_camera_order_outfit',
            path : '/pages/car/inspection-camera-order-outfit.html',
            loaded: 'carInspectionCameraOrderOutfitPageLoaded'
        },
        car_inspection_camera_order_outfit_done : {
            page : 'car_inspection_camera_order_outfit_done',
            path : '/pages/car/inspection-camera-order-outfit-done.html',
            loaded: 'carInspectionCameraOrderOutfitDonePageLoaded'
        },
        car_inspection_end : {
            page : 'car_inspection_end',
            path : '/pages/car/inspection-end.html',
            loaded: 'carInspectionEndPageLoaded'
        },
        settings:{
            page : 'settings',
            path : '/settings.html',
            loaded : 'settingsPageLoaded'
        },
        settings_manager_list:{
            page : 'settings_manager_list',
            path : '/pages/settings/settings-manager-list.html',
            loaded : 'settingsManagerListPageLoaded'
        },
        settings_manager_add:{
            page : 'settings_manager_add',
            path : '/pages/settings/settings-manager-add.html',
            loaded : 'settingsManagerAddPageLoaded'
        },
        settings_change_password:{
            page : 'settings_change_password',
            path : '/pages/settings/settings-change-password.html',
            loaded : 'settingsChangePasswordPageLoaded'
        },
        notification:{
            page : 'notification',
            path : '/notification.html',
            loaded : 'notificationPageLoaded'
        },
        custom_page:{
            page : 'custom_page',
            path : '/custom_page.html',
            loaded : 'customPageLoaded'
        }
    };
};

/**
 * Change current page if it's presented
 * @param {type} pageName
 * @returns {Boolean}
 */
TState.prototype.setPage = function(pageName){
    if(pageName in this.possiblePages) {
        this.page = pageName;
        return true;
    }
    throw ('pageName '+pageName+' is not found');
    return false;
};

TState.prototype.getPage = function(){
    return this.page;
};

TState.prototype.getPath = function(pageName){
    var name = pageName || this.page;
    if(name in this.possiblePages) {
        return this.possiblePages[name]['path'];
    }
    throw ('pageName '+pageName+' is not found');
    return false;
};

/**
 * Get name of the Function in TCore
 * @param {type} pageName
 * @param {type} evName
 * @returns {string|Boolean}
 */
TState.prototype.getFunc = function(pageName, evName){
    var name = pageName || this.page;
    var evName = evName || 'loaded'; //or default loaded func
    if(name in this.possiblePages && evName in this.possiblePages[name]) {
        return this.possiblePages[name][evName];
    }
    throw ('pageName '+pageName+' evName '+evName+' is not found');
    return false;
};

TState.prototype.back = function() {
    var prevAction = this.history.pop();
    if(prevAction.name === this.page) prevAction = this.history.pop();
    //this.save();
    if(typeof prevAction.type != 'undefined' && prevAction.type==='ajax'); //TODO for withour reload (not change page, only handlers)
    this.data = prevAction.stateData;
    core.toPage(prevAction.name);
    //window.history.back();
};

TState.prototype.save = function() {
    console.log('saving state...', 'TState:', this, 'TState.data:', this.data);
    var json = this.getForStorage();
    localStorage.setItem('TState', json);
};

TState.prototype.load = function() {
    var json = localStorage.getItem('TState');
    if(json !== null) {
        var obj = JSON.parse(json);
        Object.assign(this, obj);
    }
};

TState.prototype.pushNewHistory = function(name, type) {
    var type = type || 'usual';
   
    if(core.bDev) {
        console.log('[TState] pushNewHistory ('+name+','+type+')');
    }
    var lastItem = this.lastHistoryItem();
    if((lastItem === null || name !== lastItem.name) && name.length > 0) {
        if(core.bDev){
            console.log('[TState] adding history name '+name+', type '+type);
        }
        this.history.push({name:name,type:type,stateData:this.data});
        this.save();
    } else if(core.bDev) {
        console.log('history NOT push : this.page = '+this.page+', name = '+name+',type = '+type+', lastItem: ', lastItem);
    }
};

TState.prototype.lastHistoryItem = function() {
    return this.history.length > 0 ? this.history[this.history.length - 1] : null;
};

var TCore = function (params) {
    this.params = params || {};
    this.url = "http://logbook.localhost" ; // "http://logbook24.ru"; // "http://logbook.localhost";  //"http://profitzone.com.ua";
    this.api_url = "/api";
    this.bDev = true;
    this.bCordova = true; //Cordova initialized
    this.customPrefix = window.location.host.length > 0 && window.location.host !== "localhost:8383" ? '/platforms/android/assets/www/' : ""; //check browsersync
    this.version = '1.0.1';
    this.init();
};

TCore.prototype.init = function() {
    if(this.bDev){
        console.log('[TCore] initCore...');
    }
    window.core = this;

    jQuery.ajax({
        url: 'https://cdn.ravenjs.com/3.20.1/raven.min.js',
        dataType: 'script',
        success: function(){
            Raven.config('https://5f8e5aae27be4404a56261ed215d08ba@sentry.io/253609').install();
        },
        async: true
    });

    if(this.bDev){
        console.log('[TCore] initState...');
    }
    this.state = new TState();    //TODO load/save from storage
    
    if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/)) { //(iPhone|iPod|iPad|Android|BlackBerry)
        document.addEventListener("deviceready", function(){
            core.bCordovaInitialized = true;
            core.customPrefix = window.device && window.device.platform === "Android" ? '/android_asset/www' : '';
            core.customPrefix = window.location.host.length > 0 && window.location.host !== "localhost:8383" ?
              '/platforms/android/assets/www/' :
                    (window.device && window.device.platform === "Android" ? '/android_asset/www' : ''); //check browsersync
            core.loaded();
            if(window.device && window.device.platform === "Android") core.androidListeners();
        }, false);
    } else {
        core.bCordovaInitialized = false;
        core.loaded();
    }
    
    if($('.header__back').length) {
        document.addEventListener("backbutton", function(){
            core.state.back();
        }, false);
    }
    
    //this.loaded();
    //TODO initMenu
    
    
    window.onpopstate = function(event) {
      // "event" object seems to contain value only when the back button is clicked
      // and if the pop state event fires due to clicks on a button
      // or a link it comes up as "undefined" 

      if(event){
        // Code to handle back button or prevent from navigation
      }
      else{
        // Continue user action through link or button
      }
    };
};

TCore.prototype.hrefInit = function (){
    $('a').each(function(){
        if($(this).attr('data-page')){
            $(this).on('click', function () {
                core.toPage($(this).attr('data-page'));
            });
        }
    });
};

TCore.prototype.showLoader = function() {
    console.log('[TCore] showLoader');
    if(typeof this.bLoader != 'undefined' && this.bLoader) return;
    else this.bLoader = true;
    var loader = $('body > div.loader');
    if(loader.length < 1) {
        $.get(this.customPrefix+'/loader.html', function(data){
            $('body').append(data);
            if(window.device && window.device.platform == 'Android') {
                $('body > div.loader > img').attr('src', core.customPrefix + $('body > div.loader > img').attr('src'));
            }
        });
    } else {
        loader.removeClass('disabled')
                .css('display', 'block');
    }
    this.bLoader = false;
};

TCore.prototype.hideLoader = function() {
    $('.loader').addClass('disabled');
    setTimeout(function(){
        $('.loader').css('display', 'none');
    }, 400);

};

TCore.prototype.photoListInit = function(){
    if($(".car__photo-list").length) {
      $("[data-picture]").on("click", function(e){
        e.preventDefault();
        $("body").addClass("blur");
        $(".popup--photo").addClass("popup--active");
        $('.popup--photo img').attr('src', $(this).attr('data-picture'));
        $('.popup--photo time').html($(this).attr('data-time'));
        $('.popup--photo .popup__comment-message').html($(this).attr('data-comment'));
      });
      $(".popup__close").on("click", function(){
        $("body").removeClass("blur");
        $(".popup--photo").removeClass("popup--active");
      });
    }
};

TCore.prototype.colorsLoaded = function(data) {
    if(data.length === 0) {return};
    
    var exampleColorNode = $('.example');
    if(exampleColorNode.length === 0){return;}
    exampleColorNode = exampleColorNode[0];
    
    var listNode = $('.color-block__list');
    if(listNode.length === 0){return};
    listNode = listNode[0];

    for(var i=0; i<data.length; i++){
        var color = data[i];
        var newNode = document.createElement(exampleColorNode.nodeName);
        _newNode = $(newNode);
        newNode.id = 'color_id_'+color.id;
        newNode.className = exampleColorNode.className;
        _newNode.removeClass('example');
        newNode.innerHTML = exampleColorNode.innerHTML;
        
        _newNode.find(".color-block__radio")
                .val(color.color)
                .attr('data-name', color.name)
                .attr('data-number', color.id);
        _newNode.find('.cars__color-round')
                .css('background', color.color)
                .html(color.color);
        
        listNode.insertBefore(newNode, $('.color-block__btn')[0]);
    }
};

TCore.prototype.authorized = function(obj) {
    if(this.bDev) {
        console.log('[TCore]authorized');
    }
    this.state.token = obj.session.api_token;
    this.toPage('cars');
};

/**
 * Onload document
 * @returns {undefined}
 */
TCore.prototype.loaded = function () {
    if(core.bDev) {
        console.log('[TCore] loaded');
    }
    var page = $('body').attr('data-page'); //get page state from loaded html
    if(typeof page === 'undefined' || page === ""){return;};
    core.state.setPage(page);
    core.state.pushNewHistory(page);
    
    var func = core.state.getFunc();
    if(func.length > 0){
        core[func]();
    }else if(core.bDev){
        console.log('[TCore] function not found ...');
    }
    
    if(core.state.history.length > 0){
        $('.header__back').css('display', 'block');
    }else{
        $('.header__back').css('display', 'none');
    }
    
    $('.header__back').on('click', function(){
         core.state.back(); 
         return false;
    });
    $('.camera__btn--back').on('click', function(){
         core.state.back(); 
         return false;
    });
};

TCore.prototype.androidListeners = function() {

  console.log('[TCore] androidListeners');
  window.addEventListener('hidekeyboard', function () {
    // Describe your logic which will be run each time keyboard is shown.
    console.log('hide keyboard');
  });

  window.addEventListener('showkeyboard', function () {
    // Describe your logic which will be run each time keyboard is closed.
    console.log('show keyboard');
  });


    if(typeof AndroidFullScreen !== 'undefined')
        AndroidFullScreen.immersiveMode(false, false);
    window.addEventListener('native.keyboardshow', function (e) {
      AndroidFullScreen.showSystemUI(false, false);
      console.log('hide keyboard2');

    });
    window.addEventListener('native.keyboardhide', function (e) {
      AndroidFullScreen.immersiveMode(false, false);
      console.log('show keyboard2');
    });

};

/**
 * Prepare to init page
 */
TCore.prototype.toPage = function (name) {

    if(this.bDev){
        console.log('[TCore] toPage('+name+')');
    }

    var path = this.state.getPath(name);
    this.state.pushNewHistory(name);

    if(path.legth == "") {
        alert('Не найду такой страницы');
        this.toPage('cars');
    }
    window.location.pathname = this.customPrefix + path;
};

TCore.prototype.getPath = function () {
    //page path
    var thePath = window.location.pathname;
    if (window.device && window.device.platform == 'Android') {
        //get end
        var mainPath = this.customPrefix;
        thePath = thePath.indexOf(mainPath) >= 0 ? thePath.substr(mainPath.length) : thePath;
        return thePath;
    } else {
        //iOS
        return thePath;
    }
};

TCore.prototype.AJAXerrorHandler = function(data) {
    switch(data.status) {
        case 401:
            core.state.token = "";
            core.toPage('login');
            break;
        case 444:
            core.toPage('custom_page');
            break;
        case 445:
            alert('Доступ ограничен');
            break;
        default:
            alert(data);
    }
};