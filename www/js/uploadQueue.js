/**
 * Uploader queue. Working with core.state.queue. Will be inited to core.uploader.
 * @returns {uploadQueue}
 */
var uploaderQueue = function() {
    this.bDev = core.bDev;
    this.bUploading = false;
    core.uploader = this;
};

uploaderQueue.prototype.init = function() {
    if(this.bDev) {
        console.log('[uploadQueue] init');
    }
    setTimeout(core.uploader.handle, 1000);
};

uploaderQueue.prototype.add = function(data){
    var item = jQuery.extend(true, {bFinished: false}, data);
    core.state.uploadQueue.push(item);
};

uploaderQueue.prototype.last = function() {
    var q = core.state.uploadQueue;
    return q.length > 0 ? q[q.length - 1] : null;
};

/**
 * Handled for uploader. Can be called globally.
 * @returns {undefined}
 */
uploaderQueue.prototype.handle = function() {
    if(this.bDev) {
        console.log('[uploadQueue] handle');
    }
    
    if(this.bUploading) {
        return;
    } else {
        var item = this.last();
        this.bUploading = true;
    }
};