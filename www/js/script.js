if($("#camera-range").length) {
  $("#camera-range").rangeslider({
    polyfill: false,
    onInit: function(){
      $(".rangeslider__handle").text(0);
    },
    onSlide: function(position, value){
      $(".rangeslider__handle").text(value);
      $(".camera__photo").css("opacity", value/100);
    }
  });
  $(".camera__btn-range").on("click", function(){
    var inputRange = $("#camera-range"),
        currentValue = +inputRange.val();
    if($(this).hasClass("camera__btn-range--plus")) {
      if (currentValue < 100) {
        currentValue++;
      }
    } else {
      if (currentValue > 0) {
        currentValue--;
      }
    }
    inputRange.val(currentValue).change();
    $(".camera__photo").css("opacity", currentValue/100);
  });

  $(".camera__btn-photo-add").on("click", function(){
    if ($(this).hasClass("camera__btn-photo-add--on")) {
      $(this).removeClass("camera__btn-photo-add--on");
      $(".camera__photo").removeClass("camera__photo--on");
    } else {
      $(this).addClass("camera__btn-photo-add--on");
      $(".camera__photo").addClass("camera__photo--on").css("opacity", function(){
        $("#camera-range").val(50).change();
        return +$("#camera-range").val()/100;
      });
    }
  });

  $(".camera__btn-flash").on("click", function(){
    if ($(this).hasClass("camera__btn-flash--on")) {
      $(this).removeClass("camera__btn-flash--on");
    } else {
      $(this).addClass("camera__btn-flash--on");
    }
  });
}

if($(".camera__zoom-btn").length) {
  $(".camera__zoom-btn").on("click", function(){
    var el = $(".camera__photo-done img"),
        elWidth = parseFloat(el.css("width")),
        photoScale = 80;
    if ($(this).hasClass("camera__zoom-btn--plus")) {
      if (elWidth + photoScale >= window.innerWidth) {
        el.css("width", elWidth + photoScale);
      }

    } else {
      if (elWidth - photoScale >= window.innerWidth) {
        el.css("width", elWidth - photoScale);
      }
    }
  });
  $(".camera__btn--comment").on("click", function(){
    if ($(".camera").hasClass("camera--comment")) {
      $(".camera").removeClass("camera--comment");
      $(".comment").removeClass("comment--active");
    } else {
      $(".camera").addClass("camera--comment");
      $(".comment").addClass("comment--active");
    }
  });
}

if($(".car-inspection__comment-bg").length){
  $(".car-inspection__comment-bg").each(function(){
    var imgUrl = "url(" + $(this).closest(".car-inspection__photo").find("img").attr("src") + ")";
    $(this).css("background-image", imgUrl);
  });
}

if($(".car__accordion-btn").length) {
  $(".car__accordion-btn").on("click", function(){
    var $parent = $(this).closest(".car__accordion");
    $parent.toggleClass("car__accordion--open");
  });
}

if ($(".car__notification-link").length) {
  $(".car__notification-link").on("click", function(e){
    e.preventDefault();
    if ($(this).hasClass("car__notification-link--open")) {
      $(this).removeClass("car__notification-link--open").text("Показать");
      $(".car__notification-photo").removeClass("car__notification-photo--open");
    } else {
      $(this).addClass("car__notification-link--open").text("Свернуть");
      $(".car__notification-photo").addClass("car__notification-photo--open");
    }
  });
}

$(".car__rating-btn").on("click", function(){
  $(".car__rating-btn").removeClass("active");
  $(this).addClass("active").prevAll().addClass("active");
});

// Validation form
  function validEmpty(el) {
    var regEmail = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/i,
        regName = /^[A-Za-zА-Яа-яЁё]{2,}$/,
        type = el.attr("type"),
        $parent = el.parent(),
        val = el.val(),
        name = el.attr("name");
    switch(type) {
      case "email":
        checkEmail();
        break;
      case "text":
        // checkName();
        break;
      case "password":
        checkPassword();
        break;
    }
    function checkEmail() {
      if (val.search(regEmail) != 0) {
        invalidField(el, $parent, "E-mail не правильный");
      } else {
        validField(el, $parent);
      }
    }
    function checkPassword() {
      if (val.length == 0) {
        invalidField(el, $parent, "Пароль не правильный");
      } else {
        validField(el, $parent);
      }
    }
    function checkName() {
      if (val.search(regName) != 0) {
        invalidField(el, $parent, "Имя не правильное");
      } else {
        validField(el, $parent);
      }
    }
    function validField(el, $parent){
      el.removeClass("form__input--error");
      $parent.find(".form__message").remove();
    }
    function invalidField(el, $parent, message){
      el.addClass("form__input--error");
      $parent.find(".form__message").remove();
      $parent.append('<span class="form__message">' + message + '<svg width="18" height="18"><use xlink:href="#icon-warning"/></svg></span>');
    }
  }

// ----------------------------------------------------

// Submit on form
  // Search keyup on input
  $(".form :required").on("keyup change", function(){
    validEmpty($(this));
  });
  // ----------------------------------------------------
  $(".form [type='submit']").on("click", function(e){
    var $form = $(this).closest("form");
    $form.find(":required").each(function(){
      validEmpty($(this));
    });
    if ($form.find(".form__input--error").length != 0) {
      e.preventDefault();
    }
  });
// ----------------------------------------------------

if($(".header__menu-btn").length) {
  $(".header__menu-btn").on("click", function(){
    $(".header__menu-link").toggleClass("header__menu-link--active");
  });
}

if($("#show-hide-password").length) {
  $("#show-hide-password").on("change", function(){
    if ($(this).prop("checked")) {
      $("input:password").prop("type", "text");
    } else {
      $("input:text").prop("type", "password");
    }
  });
}

// ---------------------------------------------------

if (!Object.assign) {  //polyfill
    Object.defineProperty(Object, 'assign', {
        enumerable: false,
        configurable: true,
        writable: true,
        value: function (target, firstSource) {
            'use strict';
            if (target === undefined || target === null) {
                throw new TypeError('Cannot convert first argument to object');
            }

            var to = Object(target);
            for (var i = 1; i < arguments.length; i++) {
                var nextSource = arguments[i];
                if (nextSource === undefined || nextSource === null) {
                    continue;
                }

                var keysArray = Object.keys(Object(nextSource));
                for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
                    var nextKey = keysArray[nextIndex];
                    var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
                    if (desc !== undefined && desc.enumerable) {
                        to[nextKey] = nextSource[nextKey];
                    }
                }
            }
            return to;
        }
    });
}

// -------------------------------------------------

function JSdate($in){
    return new Date(Date.parse($in.replace('-','/','g')));
}

// ------------------------------------------------

/**
 * Convert a base64 string in a Blob according to the data and contentType.
 * 
 * @param b64Data {String} Pure base64 string without contentType
 * @param contentType {String} the content type of the file i.e (image/jpeg - image/png - text/plain)
 * @param sliceSize {Int} SliceSize to process the byteCharacters
 * @see http://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
 * @return Blob
 */
function b64toBlob(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
}

/**
 * Create a Image file according to its database64 content only.
 * 
 * @param folderpath {String} The folder where the file will be created
 * @param filename {String} The name of the file that will be created
 * @param content {Base64 String} Important : The content can't contain the following string (data:image/png[or any other format];base64,). Only the base64 string is expected.
 * @param contentType 
 * @param succesCallback
 */
function savebase64AsImageFile(folderpath,filename,content,contentType, succesCallback){
    var succesCallback = succesCallback || null;
    // Convert the base64 string in a Blob
    var DataBlob = typeof content === 'string' ? b64toBlob(content,contentType) : content;
    
    console.log("Starting to write the file :3");
    
    window.resolveLocalFileSystemURL(folderpath, function(dir) {
        console.log("Access to the directory granted succesfully");
        dir.getFile(filename, {create:true}, function(file) {
            console.log("File created succesfully.");
            file.createWriter(function(fileWriter) {
                console.log("Writing content to file");
                fileWriter.write(DataBlob);
                if(typeof succesCallback === 'function') {
                    succesCallback();
                }
            }, function(){
                alert('Unable to save file in path '+ folderpath);
            });
	});
    });
}